import peewee as pw
import datetime

# import config

# db = pw.MySQLDatabase(config.db['db'],
#                       host=config.db['host'],
#                       user=config.db['user'],
#                       password=config.db['password'])
db = pw.SqliteDatabase('tag.db')


class BaseModel(pw.Model):
    class Meta:
        database = db


class TagImage(BaseModel):
    tag = pw.CharField()
    url = pw.CharField()
    date = pw.DateTimeField()
    creator = pw.CharField()


class User(BaseModel):
    sourceId = pw.CharField()
    tipe = pw.CharField()
    session = pw.CharField(null=True)
    permission = pw.CharField(null=True)
    rank = pw.IntegerField(default=1)
    createdAt = pw.DateTimeField(default=datetime.datetime.now())
