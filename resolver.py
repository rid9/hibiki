import commands
from model import db
from markov import verniy

from linebot.models import (
    TextSendMessage
)

from util import upload_event_content

class_dictionary = commands.cmd_class


class CommandBus():
    global class_dictionary

    def run(self, api, db, verniy, command):
        cmd = command.__class__.__name__[:-7]

        handler = getattr(class_dictionary[cmd.lower()], cmd + 'Handler')
        x = handler(api, db, verniy)
        x.process(command)


bus = CommandBus()


def resolve_text(event, api):
    message = event.message.text

    # special case tag with #

    if message.count('#') == 2:
        id = message.partition('#')[2].partition('#')[0]
        message = event.message.text = '!tag ' + id

    if message.startswith('!'):
        # Validation
        # Mute validation
        # Command allow validation

        # Connect the received command with CommandBus
        cmd = message.split(' ', maxsplit=1)[0][1:]
        try:
            command = getattr(
                class_dictionary[cmd], cmd.title() + 'Command')(event)
        except Exception as e:
            api.reply_message(event.reply_token, TextSendMessage(
                text='Command not found'))
            return

        # Insert Usage logging here

        command.parse(event)

        bus.run(api, db, verniy, command)
    else:
        for line in message.splitlines():
            line = line.strip()
            if line != '':
                verniy.add_line(line)


def resolve_image(event, api):
    if event.source.type == 'user':
        api.reply_message(event.reply_token,
                          TextSendMessage(
                              text=upload_event_content(event, api)))
