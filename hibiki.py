from flask import Flask, request, abort
import random

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, ImageMessage
)
from model import db
from resolver import resolve_text, resolve_image

import config

line_bot_api = LineBotApi(config.CHANNEL_ACCESS_TOKEN)
handler = WebhookHandler(config.CHANNEL_SECRET)

app = Flask(__name__)

random.seed()


@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'


@app.route("/", methods=['GET'])
def hibikiget():
    return 'Hibiki running'


@app.before_request
def _db_connect():
    db.connect()


@app.teardown_request
def _db_close(exc):
    if not db.is_closed():
        db.close()


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    resolve_text(event, line_bot_api)


@handler.add(MessageEvent, message=ImageMessage)
def handle_image(event):
    resolve_image(event, line_bot_api)


if __name__ == "__main__":
    context = ('certificate.crt', 'private.key')
    app.run(host='0.0.0.0', port=443, debug=False, ssl_context=context)
    # app.run(host='0.0.0.0', port=5000)
