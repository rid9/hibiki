from .__base__ import Command, Handler
from random import randint


class RollCommand(Command):
    desc = {}
    desc[0] = "Usage : !roll <integer>\n"\
        "Shows a randomized number ranged from 1 to <integer>\n"\
        "Example : !roll 69"

    def parse(self, event):
        self.number = event.message.text.partition(' ')[2]

    def to_array(self):
        return {'number': self.number}


class RollHandler(Handler):

    def execute(self, command : RollCommand):
        numb = command.number
        sign = ''
        if numb[0] == '-':
            sign = '-'

        zeros = 1
        if numb.find('.') != -1:
            pos = len(numb.split('.')[1])
            zeros = 10 ** pos
            numb = (float(numb) * zeros)

        if sign != '':
            numb = int(numb) * -1

        numb = int(numb)

        result = randint(1, numb)
        result = sign + str(result/zeros)

        if (zeros == 1):
            result = str(result.split('.')[0])

        self.add_reply_text("Rolling %d sided dice teitoku~... \n \n %s"
                            % (numb, result))
        self.send_reply()
