from .__base__ import Command, Handler
import requests
from bs4 import BeautifulSoup as bs
from bs4 import SoupStrainer
import json
import random


class StalkCommand(Command):

    desc = {}
    desc[0] = "Usage : !stalk <username>\n"\
        "Posts random image from <username>'s Instagram last 12 post.\n"\
        "Please note that you cannot use this command on private account.\n"\
        "Example : !stalk mafifapip"

    def parse(self, event):
        self.username = event.message.text.partition(' ')[2]

    def to_array(self):
        return {'username': self.username}


def get_insta(username):
    try:
        r = requests.get('https://instagram.com/%s' % username)

        def valstring(str):
            return (str is not None) and (str[0] == 'w')
        soup = bs(r.text, 'lxml', parse_only=SoupStrainer('script'))

        c = soup.find(string=valstring)
        tojson = c.string[:-1].partition(' = ')[2]
        js = json.loads(tojson)['entry_data']['ProfilePage'][
            0]['user']['media']['nodes']
        num = random.randrange(len(js))
        return js[num]['thumbnail_src'], js[num]['code']
    except Exception as e:
        return -1, -1


class StalkHandler(Handler):

    def execute(self, command):
        img, c = get_insta(command.username)
        self.add_reply_image(img, img)
        self.add_reply_text('https://www.instagram.com/p/' + c + '/')
        self.send_reply()
