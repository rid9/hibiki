from .__base__ import Command, Handler


class HelpCommand(Command):
    desc = {}
    desc[0] = "Usage : !help or !help <commands>\n"\
        "Assuming that you haven't known the purpose of this command in the first place, "\
        "then you can use !help to list all the commands available or !help <command> to s"\
        "ee the details about any command you want to know more.\n"\
        "Example : !help help"

    def parse(self, event):
        self.cmd = event.message.text.partition(' ')[2]

    def to_array(self):
        return {}


class HelpHandler(Handler):

    def execute(self, command):
        from . import cmd_class
        cmd = command.cmd

        if cmd == '':
            tosend = 'Command list :\n'
            names = []
            for k, v in cmd_class.items():
                if getattr(v, k.title() + 'Command').public:
                    names.append(k)
            names.sort()
            for k in names:
                tosend += k + ', '
            tosend = tosend[:-2] + '\n'
            tosend += '''Use '!help <command>' for more details'''
        else:
            hchain = cmd.split(' ')
            try:
                desc = getattr(cmd_class[hchain[0]], hchain[
                               0].title() + 'Command').desc
                hchain = hchain[1:]
                try:
                    for i in hchain:
                        desc = desc[i]
                    tosend = desc[0]
                except:
                    tosend = 'Subcommand not found'
            except:
                tosend = 'Command not found'

        self.add_reply_text(tosend)
        self.send_reply()
