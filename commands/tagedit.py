from .__base__ import *

import time
from datetime import datetime, timezone


class TageditCommand(Command):
    desc = {}
    desc[0] = "U're not supposed to be able to read this"

    public = False

    def parse(self, event):
        self.tag, x, self.url = event.message.text.partition(' ')[
            2].partition(';')
        self.tag = self.tag.strip().lower()
        self.url = self.url.strip()

    def to_array(self):
        return {}


class TageditHandler(Handler):

    def execute(self, command):
        from model import TagImage
        tag, url = command.tag, command.url
        if url is not '':

            try:
                query = TagImage.select().where(TagImage.tag == tag).get()
                query.url = url
                query.save()
                self.add_reply_text('Edited tag "%s"' % (tag))
            except TagImage.DoesNotExist:
                t = TagImage(
                    tag=tag,
                    url=url,
                    date=datetime.now(timezone.utc),
                    creator='1')
                t.save()
                self.add_reply_text('Created tag "%s"' % (tag))
        else:
            self.add_reply_text('Error Occured')

        self.send_reply()
