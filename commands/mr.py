from .__base__ import *


class MrCommand(Command):
    desc = {}
    desc[0] = "Usage : !mr <chemical formula>\n"\
        "Calculate chemical relative formula mass.\n"\
        "Example : !mr CH3COOH"

    def parse(self, event):
        self.compound = event.message.text.partition(' ')[2]

    def to_array(self):
        return {}


class MrHandler(Handler):

    def execute(self, command):
        try:
            import requests
            from bs4 import BeautifulSoup as bs
            agent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0"
            r = requests.post(
                'http://www.webqc.org/mmcalc.php',
                data={'compound': command.compound}, headers={'User-Agent': agent})
            bs1 = bs(r.text, 'html.parser')
            tags = bs1.find_all('b')

            for y in tags:
                if y.text.find(' g/mol') != -1:
                    mr = y.text.split()[0]
            x = float(mr.split()[0])
            self.add_reply_text('Molar mass for %s is %s g/mol' %
                                (command.compound, mr.split()[0]))
        except Exception as e:
            print(e)
            self.add_reply_text("Ugh...")
        self.send_reply()
