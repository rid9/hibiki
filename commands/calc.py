from .__base__ import *
import requests
import json


class CalcCommand(Command):
    desc = {}
    desc[0] = "Usage : !calc <math syntax>\n"\
        "Performs calculation and shows the result in text. Check out https:"\
        "//web2.0calc.com/ for the syntaxes.\n"\
        "Example : !calc (5 + 3i) * (2 + 8i)"

    def parse(self, event):
        self.query = event.message.text.partition(' ')[2]

    def to_array(self):
        return {'query': self.query}


class CalcHandler(Handler):

    def execute(self, command):
        r = requests.post(
            'https://web2.0calc.com/calc',
            data={'in[]': command.query, 'trig': 'deg', 'p': '0', 's': '0'})
        hasil = r.text
        data = json.loads(hasil)
        hasil = data['results'][0]['out']
        if hasil != command.query:
            self.add_reply_text('%s = %s' % (command.query, hasil))
        self.send_reply()
