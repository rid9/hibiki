from .__base__ import Command, Handler


class ChuCommand(Command):
    desc = {}
    desc[0] = "Usage : !chu\n"\
        "CHU CHU YEAH!"

    def parse(self, event):
        pass

    def to_array(self):
        return {}


class ChuHandler(Handler):

    def execute(self, command):
        url = 'https://pbs.twimg.com/media/C2uuQhmUoAAR125.jpg'

        self.add_reply_text("CHU CHU YEAH!")
        self.add_reply_image(url, url)
        self.send_reply()
