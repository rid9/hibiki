from .__base__ import Command, Handler
from .__hidden__ import encode_message


class HideCommand(Command):
    desc = {}
    desc[0] = "Usage : !hide <message>;<to hide>\n"
    public = False

    def parse(self, event):
        self.message, self.hidden = self.get_args(event).split(';')
        self.message = self.message.strip()
        self.hidden = self.hidden.strip()

    def to_array(self):
        return {
            'message': self.message,
            'hidden': self.hidden
        }


class HideHandler(Handler):
    def execute(self, command):
        payload = encode_message(command.message, command.hidden)
        self.add_reply_text(payload)
        self.send_reply()
