from .__base__ import *


class TagsearchCommand(Command):
    desc = {}
    desc[0] = "Usage : !tagsearch <text>\n"\
        "Lists all the tag that contains <text> string. To list all the tags, leave the <text> blank.\n"\
        "Example : !tagsearch serap"

    def parse(self, event):
        self.tag = event.message.text.partition(' ')[2].strip()

    def to_array(self):
        return {}


class TagsearchHandler(Handler):

    def execute(self, command):
        tag = command.tag

        from model import TagImage

        query = TagImage.select().where(TagImage.tag.contains(tag))
        count = len(query)
        result = 'Found %d tag%s with keyword %s %s\n' % \
                 (count, 's' if count > 1 else '', tag, ':' if count > 0 else '')
        toprint = ''
        for t in query:
            toprint += ', ' + t.tag
        toprint = toprint[1:].strip()
        result += toprint

        self.add_reply_text(result)
        self.send_reply()
