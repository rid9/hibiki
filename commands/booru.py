from .__base__ import Command, Handler
import requests
from random import choice
from bs4 import BeautifulSoup as bs


class BooruCommand(Command):
    desc = {}
    desc[0] = "Usage : !booru <source> <tags>\n"\
        "Get random image under <tags> in <source>\n"\
        "Source : 'sb' - safebooru\n"

    def parse(self, event):
        self.source, self.tags = self.get_args(event).split(maxsplit=1)

    def to_array(self):
        return {}


class BooruHandler(Handler):

    def execute(self, command: BooruCommand):
        source, tags = command.source, command.tags
        if(source in ['sb', 'gelbo']):
            if(source == 'sb'):
                url = 'https://safebooru.org/index.php?page=dapi&s=post&q=index'
            elif(source == 'gelbo'):
                url = 'https://gelbooru.com/index.php?page=dapi&s=post&q=index'
            r = requests.get(url, params={'tags': tags})
            soup = bs(r.text, 'xml').find_all('post')
            c = choice(soup)
            urlc = 'https:' + c.get('sample_url')
            urlp = 'https:' + c.get('preview_url')
            urlp = requests.post('https://catbox.moe/user/api.php',
                                 data={'userhash': '', 'reqtype': 'urlupload',
                                       'url': urlp}).text
            if(urlp[:5] == 'http:'):
                urlp = 'https:' + urlp[5:]
            self.add_reply_image(urlc, urlp)
            self.add_reply_text(urlc)
        else:
            self.add_reply_text("Source {} not valid".format(command.source))

        self.send_reply()
