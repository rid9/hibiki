from .__base__ import *


class TagurlCommand(Command):
    desc = {}
    desc[0] = "Usage : !tagurl <tag>\n"\
        "Return image link of <tag>\n"\
        "Example : !tagurl o"

    def parse(self, event):
        self.id = event.message.text.partition(' ')[2]

    def to_array(self):
        return {}


class TagurlHandler(Handler):

    def execute(self, command):
        from .tag import get_tag
        url = get_tag(command.id, self.db)
        self.add_reply_text("{} : {}".format(command.id, url))
        self.send_reply()
