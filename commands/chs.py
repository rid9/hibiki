from .__base__ import *

import random


class ChsCommand(Command):
    desc = {}
    desc[0] = "Usage : !chs <items>\n"\
        "Choose from <items>, <items> are string of item, each separated with ';'\n"\
        "Example : !chs sleep;eat;watch"

    def parse(self, event):
        if event.message.text is not '':
            self.items = event.message.text.partition(' ')[2].split(';')
        else:
            self.items = []

    def to_array(self):
        return {}


class ChsHandler(Handler):

    def execute(self, command):
        items = command.items

        for i in items:
            if 'monika' in i.lower().strip():
                self.add_reply_text('Just Monika').send_reply()
                return

        if len(items) > 0:
            toprint = 'I choose ' + random.choice(items).strip()

        self.add_reply_text(toprint)
        self.send_reply()
