from .__base__ import Command, Handler
import requests

class UdCommand(Command):
    desc = {}
    desc[0] = "What are you doing here? You aren't supposed to see this. Go away!"

    public = False

    def parse(self, event):
        self.query = Command.get_args(event)

    def to_array(self):
        return {}


class UdHandler(Handler):

    def execute(self, command: UdCommand):
        r = requests.get('http://api.urbandictionary.com/v0/define',
                         params=dict(term=command.query)).json()['list'][0]
        self.add_reply_text("Top definition of '{}' from Urban Dictionary:".format(command.query))
        self.add_reply_text(r['definition'])
        self.add_reply_text('Example :\n' + r['example'])
        self.send_reply()
