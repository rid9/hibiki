from .__base__ import Command, Handler
import requests
from bs4 import BeautifulSoup as bs


class YtCommand(Command):
    desc = {}
    desc[0] = "Usage : !yt <youtube link>\n"\
        "Converts the video from <youtube link> into mp3 and then posts the "\
        "download link.\n"\
        "Example : !yt https://www.youtube.com/watch?v=gsNaR6FRuO0"

    def parse(self, event):
        self.link = Command.get_args(event)

    def to_array(self):
        return {}


def get_yt(link):
    from util import caturl

    yt_link = 'https://www.youtubeinmp3.com/fetch/?video=%s' % link
    r = requests.get(link)
    bs1 = bs(r.text, 'html.parser')
    title = bs1.find('title').text.partition(' - YouTube')[0]
    url = caturl(yt_link)

    return url, title


class YtHandler(Handler):

    def execute(self, command):
        url, title = get_yt(command.link)
        self.add_reply_text(title + '\n' + 'Link : ' + url)
        self.send_reply()
