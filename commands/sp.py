from .__base__ import Command, Handler


class SpCommand(Command):
    public = False
    desc = {}
    desc[0] = "Usage : !call\n"\
        "A simple call"

    def parse(self, event):
        self.origurl = self.get_args(event)

    def to_array(self):
        return {}


class SpHandler(Handler):
    def execute(self, command: SpCommand):
        spurl = 'https://s.4cdn.org/image/spoiler-a1.png'
        self.add_reply_image(command.origurl, spurl)
        self.send_reply()
