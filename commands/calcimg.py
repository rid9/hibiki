from .__base__ import Command, Handler
import requests
import json
import base64


class CalcimgCommand(Command):
    desc = {}
    desc[0] = "Usage : !calcimg <math syntax>\n"\
        "Performs calculation and shows the result in image format. Check"\
        " out https://web2.0calc.com/ for the syntaxes.\n"\
        "Example : !calcimg (5 + 3i) * (2 + 8i)"

    def parse(self, event):
        self.query = event.message.text.partition(' ')[2]

    def to_array(self):
        return {'query': self.query}


class CalcimgHandler(Handler):

    def execute(self, command):
        r = requests.post(
            'https://web2.0calc.com/calc',
            data={'in[]': command.query, 'trig': 'deg', 'p': '0', 's': '0'})
        hasil = r.text
        data = json.loads(hasil)
        hasil = data['results'][0]['img64']
        decode = base64.decodestring(hasil.encode())
        r = requests.post(
            'https://catbox.moe/user/api.php',
            files={
                'reqtype': (None, 'fileupload'),
                'userhash': (None, ''),
                'fileToUpload': decode})
        imglink = r.text
        self.add_reply_image(imglink, imglink)
        self.send_reply()
