from .__base__ import Command, Handler


class LeaveCommand(Command):
    desc = {}
    desc[0] = "Usage : !leave\n"\
        "Make Hibiki leave the group/room"

    def parse(self, event):
        pass

    def to_array(self):
        return {}


class LeaveHandler(Handler):

    def execute(self, command):
        url = 'https://files.catbox.moe/fug4j5.jpg'
        self.add_reply_image(url, url)
        self.add_reply_audio('https://files.catbox.moe/kycwv8.m4a', 6913)
        self.add_reply_text(
            "My true name is Hibiki... \nДо свидания... Goodbye. ")
        self.send_reply()
        self.leave_current_room()
