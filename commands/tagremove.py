from .__base__ import *


class TagremoveCommand(Command):
    desc = {}
    desc[0] = "No."

    public = False

    def parse(self, event):
        self.tag = event.message.text.partition(' ')[2]

    def to_array(self):
        return {}


class TagremoveHandler(Handler):

    def execute(self, command):
        from model import TagImage
        tag = command.tag
        try:
            query = TagImage.select().where(TagImage.tag == tag).get()
            query.delete_instance()
            self.add_reply_text("Removed " + tag)
        except Exception as e:
            self.add_reply_text("Error occured")
        self.send_reply()
