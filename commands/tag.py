from .__base__ import *


class TagCommand(Command):
    desc = {}
    desc[0] = "Usage : !tag <tag1>[;<tag2>;<tag3>....]\n"\
        "Return <tag1>, <tag2>, etc.\n"\
        "Example : !tag o"

    def parse(self, event):
        self.ids = event.message.text.partition(' ')[2].split(';')

    def to_array(self):
        return {}


def get_tag(id, db):
    from model import TagImage
    for c in id:
        if c == '\n':
            return 0
    try:
        query = TagImage.select().where(TagImage.tag == id).get()
        return query.url
    except:
        return -1


class TagHandler(Handler):

    def execute(self, command):
        ids = command.ids

        for id in ids:
            url = get_tag(id, self.db)
            if url is not -1:
                self.add_reply_image(url, url)
            else:
                self.add_reply_text("Ugh...")

        self.send_reply()
