from .__base__ import Command, Handler
import random


class RynCommand(Command):
    desc = {}
    desc[0] = "Usage : !ryn <question>\n"\
        "Answer <question> (Yes/No/Maybe)\n"\
        "Example : !ryn do i have to do that?"

    def parse(self, event):
        self.question = event.message.text[1:].partition(' ')[2]

    def to_array(self):
        return {'question': self.question}


class RynHandler(Handler):

    def execute(self, command : str):
        if 'monika' in command.question.lower().strip():
            x = random.randint(1, 10)
            if x == 1:
                monika = 'https://files.catbox.moe/4ndlui'
                self.add_reply_image(monika, monika).send_reply()
            else:
                toprint = '''J͍̩̥̝̟̦̙̾͌ͨͣ͘Ṵ̭͖͖̜̦͗ͅṢ̵̶̛̬͙̘̰͇͂̊T̴̴̝̪̞̮̖̤̥ͬ͒̓͒ ̮̗̦̦ͦ͠M̨̙̥͕̖̙̲̥ͬ͒͌͒O͍̥̳͎̟̪̙͗ͤ̕͢ͅN̛̫͕͉ͨ̊́͝I̷̞̞̟̅̈́̎ͦͧͧ́K̛̀͂͂ͦ̕͏͔̤̞̮͍̜͙A̳̬̦̠̩ͥ͂̽͛̎ͯͮͭ͜͠'''
                toprint += '\n\n Yes, gladly'
                self.add_reply_text(toprint).send_reply()
            return


        choicelst = ['Yes', 'No', 'Maybe']
        toprint = command.question + '\n\n' + random.choice(choicelst)
        if command.question == '':
            toprint = random.choice(choicelst)
        self.add_reply_text(toprint)
        self.send_reply()
