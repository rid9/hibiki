import json
import requests

from .__base__ import Command, Handler


class BtcCommand(Command):
    desc = {}
    desc[0] = "Usage : !btc\n"\
       "Get the latest bitcoin price from multiple sources\n"\
       "Example : !btc"

    def parse(self, event):
        pass

    def to_array(self):
        return {}


class BtcHandler(Handler):

    def execute(self, command : BtcCommand):
        data = json.loads(requests.get(
            'http://preev.com/pulse/units:btc+usd/sources:bitfinex+bitstamp+btce').text)
        bitfinex = data['btc']['usd']['bitfinex']['last']
        bitstamp = data['btc']['usd']['bitstamp']['last']
        # btce = data['btc']['usd']['btce']['last']
        msg = 'Current BTC Price : \nBitFinex = {} USD\nBitStamp = {} USD'.format(bitfinex, bitstamp)
        self.add_reply_text(msg)
        self.send_reply()
