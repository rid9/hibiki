from .__base__ import Command, Handler


class InfoCommand(Command):
    desc = {}
    desc[0] = "Usage : !info\n"\
        "Shows bot creator's name."

    def parse(self, event):
        pass

    def to_array(self):
        return {}


class InfoHandler(Handler):

    def execute(self, command):
        self.add_reply_text(
            "Made for personal satisfaction by : \n"
            "Gabriel B. Raphael(line:blxcklisted) & Ridho Pratama(line:rid9)\n"
            "Contributions by : Dichi, Kenoichi Adi")
        self.send_reply()
