from .__base__ import Command, Handler
import json
import requests


class WikiCommand(Command):
    desc = {}
    desc[0] = "Usage : !wiki <lang> <title>\n"\
        "Posts the first paragraph of a wikipedia article in your desired "\
        "language. Use abbreviation used by Wikipedia for the <lang> tag. "\
        "List of languages : https://meta.wikimedia.org/wiki/List_of_Wikip"\
        "edias\nExample : !wiki en japanese destroyer hibiki"

    def parse(self, event):
        self.lang, _, self.title = Command.get_args(event).partition(' ')
        self.url = 'https://' + self.lang.strip() + '.wikipedia.org/w/api.php'

    def to_array(self):
        return {}


class WikiHandler(Handler):

    def execute(self, command):
        # Make Payload
        payload = {'generator': 'search', 'gsrsearch': command.title.strip(),
                   'gsrlimit': '1', 'action': 'query', 'format': 'json',
                   'prop': 'extracts', 'exintro': '', 'explaintext': '',
                   'redirects': '1'}
        # Send
        j = json.loads(requests.get(command.url, params=payload).text)
        j = j['query']['pages']
        for k in j:
            self.add_reply_text(j[k]['extract'].splitlines()[0])
            break
        self.send_reply()
