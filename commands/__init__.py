from os.path import dirname, basename, isfile
import glob
from importlib import import_module

modules = glob.glob(dirname(__file__)+"/*.py")

#__all__ = [ basename(f)[:-3] for f in modules if isfile(f) and not basename(f).startswith('__')]

# print(__all__)

__all__ = []
cmd_class = {}

for f in modules:
    if isfile(f) and not basename(f).startswith('__') and f.endswith('.py'):
        name = basename(f)[:-3]
        # try:
        __all__ += [name]
        cmd_class[name] = import_module('.' + basename(f)[:-3], 'commands')
        assert hasattr(cmd_class[name], name.title() + 'Command')
        assert hasattr(cmd_class[name], name.title() + 'Handler')
        print("Imported command {}".format(name))
        # except:
        #    print(name + ' is not a valid command file.')
