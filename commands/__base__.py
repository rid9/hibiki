from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage, ImageMessage, ImageSendMessage,
    AudioSendMessage
)


class Command():
    allow = {}
    allow['user'] = True
    allow['group'] = True
    allow['room'] = True

    public = True

    desc = {0: 'Base command'}

    def __init__(self, event):
        self.event = event

    def to_array(self):
        raise Exception('Command Not Found')

    @staticmethod
    def get_args(event):
        return event.message.text.partition(' ')[2]

    def parse(self, event):
        raise Exception('This method should be there')


class Handler():

    def __init__(self, api, db, verniy):
        self.data = []
        self.api = api
        self.db = db
        self.verniy = verniy

    def add_reply_text(self, text):
        self.data.append(('text', text))
        return self

    def add_reply_image(self, orig, prev):
        self.data.append(('image', orig, prev))
        return self

    def add_reply_audio(self, orig, duration):
        self.data.append(('audio', orig, duration))
        return self

    def send_reply(self):
        if len(self.data) <= 5:
            to_send = []
            for d in self.data:
                if d[0] == 'text':
                    to_send.append(TextSendMessage(text=d[1]))
                elif d[0] == 'image':
                    to_send.append(ImageSendMessage(
                        original_content_url=d[1], preview_image_url=d[2]))
                elif d[0] == 'audio':
                    to_send.append(AudioSendMessage(
                        original_content_url=d[1], duration=d[2]))
            self.api.reply_message(self.event.reply_token, to_send)
        else:
            self.api.reply_message(
                self.event.reply_token, TextSendMessage(text='Ugh...'))

    def leave_current_room(self):
        if self.event.source.type == 'group':
            sid = self.event.source.group_id
            self.api.leave_group(sid)
        elif self.event.source.type == 'user':
            sid = self.event.source.user_id
        else:
            sid = self.event.source.room_id
            self.api.leave_room(sid)

    def get_reply(self):
        return self.data

    def process(self, command):
        self.event = command.event
        self.execute(command)

    def execute(self, command):
        raise Exception('Handler Not Found')
