from .__base__ import Command, Handler
from .__hidden__ import decode_message


class UnhideCommand(Command):
    desc = {}
    desc[0] = "Usage : !unhide <message>\n"
    public = False

    def parse(self, event):
        self.message = self.get_args(event).strip()

    def to_array(self):
        return {
            'message': self.message,
        }


class UnhideHandler(Handler):
    def execute(self, command):
        try:
            payload = decode_message(command.message)
            self.add_reply_text(payload)
        except:
            self.add_reply_text("[There are no hidden message in here!]")
        self.send_reply()
