zwsp = u'\u200b'
zwnj = u'\u200c'
zwj = u'\u200d'


def str_to_binary(string):
    return ''.join([bin(x)[2:].zfill(8) for x in string.encode('utf8')])


def binary_to_str(binary):
    return bytes([int(binary[i:i + 8], 2) for i in range(0, len(binary), 8)])\
        .decode('utf8')


def binary_to_zerowidth(binary):
    hasil = []
    hasil.append(zwj)
    for c in binary:
        if c == '0':
            hasil.append(zwsp)
        else:
            hasil.append(zwnj)
    hasil.append(zwj)
    return ''.join(hasil)


def zerowidth_to_binary(zw):
    assert zw[0] == zwj and zw[-1] == zwj
    zw = zw[1:-1]
    hasil = ''
    for c in zw:
        if c == zwsp:
            hasil += '0'
        elif c == zwnj:
            hasil += '1'
    return hasil


def encode_message(message, hidden):
    zw = str_to_binary(hidden)
    zw = binary_to_zerowidth(zw)
    return message + zw


def decode_message(message):
    hidden = ''
    for c in message:
        if c in [zwsp, zwnj, zwj]:
            hidden += c
    hidden = zerowidth_to_binary(hidden)
    hidden = binary_to_str(hidden)
    return hidden
