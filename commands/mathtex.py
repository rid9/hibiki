from .__base__ import Command, Handler
from urllib.parse import quote


class MathtexCommand(Command):
    desc = {}
    desc[0] = "Usage : !mathtex <query>\n"\
        "CHU CHU YEAH!"
    public = False

    def parse(self, event):
        self.query = Command.get_args(event)

    def to_array(self):
        return {}


class MathtexHandler(Handler):

    def execute(self, command):
        base_url = 'https://latex.codecogs.com/png.download?'
        url = base_url + quote(r'\dpi{300} \bg_white \large ' + command.query)
        self.add_reply_image(url, url)
        # self.add_reply_text(url)
        self.send_reply()
