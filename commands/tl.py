from .__base__ import Command, Handler
import requests

class TlCommand(Command):
    desc = {}
    desc[0] = "Usage : !tl <sourceLang> <destLang> <text>\n" \
              "Translates <text> from the source language into destination language. Use ISO 639-1 code for the " \
              "<sourceLang> and <destLang>. You are also able to use 'auto' as the <sourceLang> to autodetect the " \
              "text language.\n" \
              "Example : !tl id en Saya seorang weeaboo"

    def parse(self, event):
        self.source, self.dest, self.text = Command.get_args(event).split(' ', maxsplit=2)

    def to_array(self):
        return {}


class TlHandler(Handler):

    def execute(self, command : TlCommand):

        url = "https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&sl="
        payload = {'sl': command.source.strip(), 'tl': command.dest.strip(), 'q': command.text}
        headers = {'Host': 'translate.googleapis.com',
                   'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',
                   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
        r = requests.get(url, params=payload, headers=headers)
        tosend = ''
        for c in r.json()[0]:
            tosend += c[1].strip() + ' -> ' + c[0].strip() + '\n\n'
        tosend = tosend.strip()
        self.add_reply_text(tosend)
        self.send_reply()
