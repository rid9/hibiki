from .__base__ import *


class CallCommand(Command):
    desc = {}
    desc[0] = "Usage : !call\n"\
        "A simple call"
    desc['test'] = {}
    desc['test'][0] = 'Weh ketemu'

    def parse(self, event):
        pass

    def to_array(self):
        return {}


class CallHandler(Handler):

    def execute(self, command):
        self.add_reply_text(
            "Roger, Hibiki, heading out!\n\nI'll never forget Tenshi...")
        self.send_reply()
