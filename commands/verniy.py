from .__base__ import Command, Handler


class VerniyCommand(Command):
    desc = {}
    desc[0] = "VERNIY\n"
    public = False

    def parse(self, event):
        pass

    def to_array(self):
        return {}


class VerniyHandler(Handler):

    def execute(self, command):
        s = self.verniy.generate_line()
        self.add_reply_text('"' + s + '"')
        self.send_reply()
