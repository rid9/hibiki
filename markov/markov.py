from .model import Pair, Word
import peewee as pw
import os


class Markov:
    def new_from_file(self, fname):
        # create new sqlite db file
        self.new_blank(fname)

        with self.db.atomic():
            self.add_file(fname)

    def new_blank(self, fname):
        try:
            os.remove(fname)
        except FileNotFoundError:
            pass
        self.db = pw.SqliteDatabase(fname)
        self.db.connect(reuse_if_open=True)

        self.Pair = _set_model(Pair, self.db)
        self.Pair.create_table()
        self.Word = _set_model(Word, self.db)
        self.Word.create_table()

    # @profile
    def add_line(self, line):
        line = '$START$ ' + line + ' $END$'
        line = line.split()
        l_len = len(line)
        kata1, _ = self.Word.get_or_create(kata=line[0])
        for i in range(0, l_len - 1):
            kata2, _ = self.Word.get_or_create(kata=line[i + 1])

            if len(line[i]) > 32 or len(line[i + 1]) > 32:
                kata1 = kata2
                continue

            try:
                q = self.Pair.update(count=self.Pair.count + 1)\
                    .where(self.Pair.kata1 == kata1)\
                    .where(self.Pair.kata2 == kata2)\
                    .execute()
                assert q != 0
            except AssertionError:
                self.Pair(kata1=kata1, kata2=kata2,
                          count=1).save(force_insert=True)
            kata1 = kata2

    def add_file(self, fname):
        with open(fname) as f, self.db.atomic():
            for l in f.readlines():
                l = l[:-1].strip()
                if l == '':
                    continue
                self.add_line(l)

    def load_from_db(self, dbname):
        try:
            self.db = pw.SqliteDatabase(dbname)
            self.Pair = _set_model(Pair, self.db)
            self.Word = _set_model(Word, self.db)
            self.db.connect()
        except FileNotFoundError:
            print("Can't find file {}".format(dbname))

    # @profile
    def generate_line(self):
        from random import randint
        words = self.Word.select()
        start = self.Word.get(self.Word.kata == '$START$')
        end = self.Word.get(self.Word.kata == '$END$')
        ids = [start.id]
        hasil = []

        while True:
            while ids[-1] != end.id:
                pairs = self.Pair.select()\
                    .where(self.Pair.kata1_id == ids[-1])\
                    .order_by(self.Pair.count)
                count = pairs.select(pw.fn.SUM(self.Pair.count)).scalar()
                if count == 0:
                    ids.append(end.id)
                    break
                count = randint(0, count)
                for pair in pairs.dicts():
                    count -= pair['count']
                    if count <= 0:
                        ids.append(pair['kata2'])
                        break
            for id in ids[1:-1]:
                hasil.append(words.where(self.Word.id == id).get().kata)

            if len(hasil) < 5:
                ids = [start.id]
                continue

            break
        return ' '.join(hasil)


def _set_model(c, db):
    r = c
    r._meta.database = db
    return r
