import peewee as pw


class Word(pw.Model):
    kata = pw.CharField(unique=True, max_length=32)


class Pair(pw.Model):
    kata1 = pw.ForeignKeyField(Word)
    kata2 = pw.ForeignKeyField(Word)
    count = pw.IntegerField(default=0)

    class Meta:
        primary_key = pw.CompositeKey('kata1', 'kata2')


if __name__ == '__main__':
    db = pw.SqliteDatabase('test.db')
    db.connect()
    Word._meta.database = db
    Pair._meta.database = db
    db.create_tables([Word, Pair], safe=True)
