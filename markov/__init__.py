from .markov import Markov
import os

verniy = Markov()
if os.path.isfile('verniy.db'):
    verniy.load_from_db('verniy.db')
else:
    verniy.new_blank('verniy.db')
