# coding=utf-8

import requests
import json
import re
from datetime import datetime, timedelta, timezone

from random import randint


def caturl(link):
    try:
        r = requests.post(
            'https://qt.catbox.moe/shortener.php',
            data={'url': link})
        return r.text
    except:
        return -1


def bitly(link):
    try:
        r = requests.get(
            'https://api-ssl.bitly.com/v3/shorten?access_token=f5f3f749574d4be'
            '344708d8b2526efc14651ba9f&longUrl=%s&format=txt' % (link))
        return r.text
    except:
        return -1


def get_insta(username):
    try:
        r = requests.get('https://instagram.com/%s' % username)
        reg = re.compile(
            r'''("thumbnail_src":\s"([a-zA-Z0-9._%&/:=+-?]+)")''', re.VERBOSE)
        return reg.findall(r.text)[randint(0, len(reg.findall(r.text)) - 1)][1]
    except:
        return -1


def get_nim(nim):
    try:
        if len(nim) == 8:
            r = requests.get('https://nim.arc.itb.ac.id/search/%s/0/1' % nim)
            data = json.loads(r.text)
            return 'Nama: %s \nProdi: %s \nTahun Angkatan: %s' % \
                (data['data'][0]['name'], data['data'][0]['prodi']['name'],
                 data['data'][0]['year'])
        else:
            return 'NIM length must be 8 digits'
    except:
        return 'Error Occured. Please try again'


def log(event, msg=None):
    if msg is None:
        msg = event.message.text
    if event.source.type == 'group':
        sid = 'g_' + event.source.group_id
    elif event.source.type == 'user':
        sid = 'u_' + event.source.user_id
    else:
        sid = 'r_' + event.source.room_id

    with open('log/' + sid, 'a', encoding='utf-8') as f:
        dt = datetime.now(timezone(timedelta(hours=7)))
        f.write(dt.strftime('%Y-%m-%d %H:%M:%S') + ' UTC+7 | ' + msg + '\n')


def upload_event_content(event, api):
    ''' upload gambar from ImageEvent
    return the url if succeed, 'Ugh..' if fail
    '''
    try:
        gambar = api.get_message_content(event.message.id).content
        r = requests.post(
            'https://catbox.moe/user/api.php',
            files={
                'reqtype': (None, 'fileupload'),
                'userhash': (None, ''),
                'fileToUpload': ('hibiki.png', gambar, 'image/png')})
        imglink = r.text
        return imglink
    except:
        return 'Ugh...'


def error_logging(event, db, e):
    dt = datetime.now(timezone(timedelta(hours=7)))
    with open('log/error_log' + dt.strftime('%Y%m') + '.txt', 'a',
              encoding='utf-8') as f:
        f.write(dt.strftime('%Y%m%d') + '\n')
        try:
            f.write(event.message.text + '\n')
        except:
            pass
        f.write(str(e) + '\n')
        f.write('====================================\n')
